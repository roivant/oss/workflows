# Makefile for workflows

# Print usage of main targets when user types "make" or "make help"
help:
	@echo -e "Please choose one of the following targets:\
	\n   setup: initialize your development environment\
	\n   dependencies: install dependencies\
	\n   test: run tests\
	"

PYTHON_VERSION=3.8.7
REPO_NAME=workflows

# Sets up a virtualenv that is managed by pyenv
.PHONY: pyenv
pyenv:
ifneq (${CI}, true)
	pyenv install -s ${PYTHON_VERSION}
	@[ ! -e ~/.pyenv/versions/${REPO_NAME} ] && pyenv virtualenv ${PYTHON_VERSION} ${REPO_NAME} || true
	pyenv local ${REPO_NAME}
endif


# Install project dependencies
.PHONY: dependencies
dependencies:
	pip install -U "pip<21.3" temple
	pip install -r test_requirements.txt
	pip install -e ".[scheduler,k8s,bq]"
	pip check


# Compile requirements files
export CUSTOM_COMPILE_COMMAND=make compile_dependencies
.PHONY: compile_dependencies
compile_dependencies:
	pip-compile test_requirements.in --no-emit-index-url
	pip-compile requirements.in --no-emit-index-url


# Setup a development environment
.PHONY: setup
setup: pyenv dependencies
	pre-commit install


# Run tests
.PHONY: test
test:
	coverage run -m pytest --junit-xml report.xml
	coverage report --fail-under=100


# Validate formatting and typechecking
.PHONY: validate
validate:
	pre-commit run --all-files
	cd docs/ && make html


# Clean generated files
.PHONY: clean
clean:
	rm -rf dist/ build/ .pytest_cache/ *.egg-info .coverage report.xml docs/_build


# Build and publish a dev package
.PHONY: dev_build
dev_build: clean
	python setup.py bdist_wheel
	twine upload --verbose dist/*
