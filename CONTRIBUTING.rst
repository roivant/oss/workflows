Contributing
============

Licensing
---------

Contributions are made under the terms of the Apache License, Version 2.0.
See `LICENSE <LICENSE>`_.

Code of conduct
---------------

Contributors must adhere to the Contributor Covenant `Code of Conduct
<https://www.contributor-covenant.org/version/2/0/code_of_conduct/>`_.
Please report suspected violations to vant.tech.eng@roivant.com.

Opening issues
--------------

You may open an issue on the GitLab repository in order to report a bug,
start a discussion on a feature, or otherwise contribute.

Submitting merge requests
-------------------------

You may fork the repository and open a merge request to contribute changes.
See the `README <README.rst>`_ for details on setting up your development
environment.

Development setup
-----------------

This project uses `pyenv <https://github.com/yyuu/pyenv>`_, please install it first.
When running on a Mac, the ``Makefile`` uses Homebrew to install dependencies. On other
platforms please install them manually.

Clone the repo and setup your development environment by running::

    git clone https://gitlab.com/roivant/oss/workflows.git
    make setup

Create a PostgreSQL database to run tests against::

    brew install postgresql
    brew services start postgresql
    createdb workflows

Run the tests and code validations::

    make test
    make validate

Run the example dag, providing a database connection::

    export DATABASE_URL=postgres://localhost/workflows
    workflows example_project.config run trader_bot


About the template
-------------------

This repository was created from the `temple-python <https://gitlab.com/roivant/vant-tech/temple-python>`_
template. If you have access to that repository, apply updates from the template by running::

    temple update

What tools are included?
~~~~~~~~~~~~~~~~~~~~~~~~

- A Makefile for convenience; use ``make setup`` to setup your dev environment
- A build configuration in ``.gitlab-ci.yml``
- A test framework using pytest and requiring 100% coverage; use ``make test`` to run
- Python auto-formatting using ``black``, ``flake8``, and ``isort`` via a git pre-commit hook
- Automatic versioning and ChangeLog using `PBR <https://docs.openstack.org/developer/pbr/>`_

Versioning using PBR
~~~~~~~~~~~~~~~~~~~~

The `PBR <https://docs.openstack.org/developer/pbr/>`_ library will automatically
increment the version when you commit to the main branch by merging your pull request.
The commit message that you enter on GitHub when merging will determine what version
number is assigned, using `Semantic Versioning <http://semver.org/>`_.

- Messages starting with ``Sem-Ver: bugfix,`` or with no matching message will bump the ``PATCH`` number
- Messages starting with ``Sem-Ver: feature,`` or ``Sem-Ver: deprecation,`` will bump the ``MINOR`` number.
- Messages starting with ``Sem-Ver: api-break,`` will bump the ``MAJOR`` number.
