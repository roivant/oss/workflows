import pytest
from django.conf import settings
from django.db import connection


@pytest.mark.django_db
@pytest.fixture()
def dsn():
    yield "/".join([*settings.DATABASE_URL.split("/")[:-1], connection.settings_dict["NAME"]])
