"""
Create the migrations using:
DJANGO_SETTINGS_MODULE=example_project.settings django-admin makemigrations example_project
"""
from django.db import models


class Person(models.Model):
    name = models.TextField()
    mother = models.ForeignKey("Person", on_delete=models.PROTECT, related_name="child")
    cousin = models.ManyToManyField("Person", symmetrical=True)
