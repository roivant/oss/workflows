/*
description: |
  Example reference across DAGs.
type: sql
inputs: []
outputs:
- type: table
  name: limits
  columns:
  - name: max_bid
    datatype: integer
    nullable: true
 */
DROP TABLE IF EXISTS limits;

CREATE TABLE limits AS
SELECT 100 as max_bid;
