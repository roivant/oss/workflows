from example_project import config


def test_other_dag(workflows):
    workflows.execute_dag(config.DAGS["other_dag"])
    # The value 100 is hardcoded in the transform `transform.sql` and has no special meaning
    assert workflows.execute_sql("SELECT * FROM limits")[0][0] == 100
