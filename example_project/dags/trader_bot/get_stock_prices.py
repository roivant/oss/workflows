"""
description: |
  Get stock prices from an API.
type: python
parameters:
- DATABASE_URL
inputs: []
outputs:
- type: table
  name: stock_prices
  columns:
  - name: date
    datatype: date
  - name: close
    datatype: numeric
"""
import psycopg2.extras
import requests


def run(context):
    url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=IBM&apikey=demo"
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    cursor = context["conn"].cursor()
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS stock_prices (date date not null, close numeric not null)"
    )
    statement = "INSERT INTO stock_prices VALUES %s"
    values = [(date, value["4. close"]) for date, value in data["Time Series (Daily)"].items()]
    psycopg2.extras.execute_values(cursor, statement, values)
