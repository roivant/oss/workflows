/*
description: |
  Read stock price changes and output trades for execution.
type: sql
inputs:
- get_stock_prices.py
- other_dag:transform.sql
outputs:
- type: table
  name: new_trades
  columns:
  - name: date
    datatype: date
    nullable: true
  - name: limit_price
    datatype: numeric
    nullable: true
  validation_queries:
  - SELECT * FROM new_trades WHERE limit_price < 0
 */
DROP TABLE IF EXISTS new_trades;

CREATE TABLE new_trades AS
SELECT date, close + max_bid as limit_price
FROM stock_prices
CROSS JOIN limits;
