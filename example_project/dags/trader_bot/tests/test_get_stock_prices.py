import pytest
import responses
from django.db import connection

from example_project import config


@responses.activate
@pytest.mark.django_db
def test_get_stock_prices(workflows):
    task = config.DAGS["trader_bot"].get_task("get_stock_prices.py")
    responses.add(
        responses.GET,
        "https://www.alphavantage.co/query",
        json={
            "Time Series (Daily)": {
                "2021-02-17": {
                    "1. open": "119.2700",
                    "2. high": "120.5600",
                    "3. low": "119.0200",
                    "4. close": "119.9700",
                    "5. volume": "3949876",
                },
            }
        },
    )

    workflows.execute_task(task)

    cursor = connection.cursor()
    cursor.execute("select count(*) from stock_prices")
    assert cursor.fetchone()[0] == 1
