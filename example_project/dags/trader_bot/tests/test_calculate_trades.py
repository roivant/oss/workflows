from example_project import config


def test_calculate_trades(workflows):
    task = config.DAGS["trader_bot"].get_task("calculate_trades.sql")
    initial_data = {
        "stock_prices": [{"date": "2020-01-01", "close": 34}, {"date": "2020-01-02", "close": 43}],
        "limits": [{"max_bid": 100}],
    }
    workflows.setup_task_inputs(task, initial_data)
    results = workflows.execute_task(task)
    assert len(results["new_trades"]) == 2
    assert results["new_trades"][0]["limit_price"] == 134
