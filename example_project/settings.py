import os

import dj_database_url

DATABASE_URL = os.environ.setdefault("DATABASE_URL", "postgres://localhost/workflows")
DATABASES = {"default": dj_database_url.parse(DATABASE_URL)}
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

INSTALLED_APPS = ["example_project", "workflows"]

SECRET_KEY = "JUST A TEST FOLKS"

TIME_ZONE = "UTC"
USE_TZ = True
