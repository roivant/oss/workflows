import os

import django

import workflows.dags

# setup django. required only if using django models.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "example_project.settings")
django.setup()

# register the DAGs folder
directory = os.path.join(os.path.dirname(__file__), "dags")
DAGS = workflows.dags.register_directory(directory)
DAGS.worker_config = {
    "in_cluster": os.environ.get("KUBE_IN_CLUSTER", False),
    "image": os.environ.get("KUBE_IMAGE", "python:3.8-slim-buster"),
}
