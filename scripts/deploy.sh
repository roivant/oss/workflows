#!/bin/bash
# Deployment script
# - get the versions from FBR
# - tag the git repo with the version
# - update version.py to the version
# - create a wheel and push to artifact server
# - commit the ChangeLog and AUTHORS files
# - push to github

# sed is not cross-platform. run it in a container instead:
# docker run -v $PWD:/app -it --rm python:3.8-buster bash
# export CI_COMMIT_BRANCH=main TWINE_PASSWORD=xyz

# fail on error
set -e

version=$(python setup.py --version | sed -n -E 's/^([0-9]+.[0-9]+.[0-9]+).*/\1/p')
git config --global user.email build-system@roivant.com
git config --global user.name "Build System"

echo "Tagging version $version"
git checkout $CI_COMMIT_BRANCH
git remote add gitlab git@gitlab.com:roivant/oss/workflows.git || true
mkdir -p ~/.ssh
printf '%s\n' "$GITLAB_KEY" > ~/.ssh/id_ed25519
chmod 0600 ~/.ssh/id_ed25519
echo 'gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9' > ~/.ssh/known_hosts
git tag -f -a "$version" -m "Version $version"
sed -i "s/^__version__ = .*/__version__ = \"$version\"/" ./*/version.py

echo "Building wheel"
python setup.py bdist_wheel
twine upload --verbose dist/*

echo "Commit and push"
git add ChangeLog AUTHORS ./*/version.py
git commit -m "PBR version bump [skip ci]"
git push --follow-tags gitlab

echo "🍾 Deployment complete 🍾"
