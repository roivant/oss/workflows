import abc
import hashlib
import json
import logging
import os
import re
import signal
import socket
import subprocess
import typing

from django.db import connection, transaction

from workflows import bases, models

LOGGER = logging.getLogger(__name__)
MANAGERS = {}


def try_advisory_lock(name):
    """
    Take a lock to indicate the scheduler is running. Released when the
    connection is dropped. This converts a string into a bigint number
    that is relatively unique; however there is no way to ensure that
    other applications on the same database are using a different lock.
    """
    number = int("".join([str(ord(letter)) for letter in name])[:18])
    cursor = connection.cursor()
    cursor.execute("select pg_try_advisory_lock(%s)", (number,))
    return cursor.fetchall()[0][0]


class TaskManager(abc.ABC):
    """Interface for different task manager implementations"""

    def __init_subclass__(cls, **kwargs):
        """Register subclasses so they can be found by name"""
        assert cls not in MANAGERS, f"Attempted to register {cls.__name__} twice"
        MANAGERS[cls.__name__.lower()] = cls
        super().__init_subclass__(**kwargs)

    def __init__(self, registry: bases.DagRegistry):
        self.registry = registry

    def get_worker_config(self, task_run):
        """
        Return the Pod configuration for a given task, generated by merging the configurations
        at the registry, DAG, and task levels. This allows, for example, a default resource limit
        and a task-level override.
        """
        return {
            **self.registry.worker_config,
            **self.registry[task_run.dag_run.dag].worker_config,
            **self.registry[task_run.dag_run.dag].get_task(task_run.task).worker_config,
        }

    @abc.abstractmethod
    def start_task(self, task_run: models.TaskRun):
        """Start a task running"""
        pass

    @abc.abstractmethod
    def update_tasks(self, running_tasks: models.models.QuerySet) -> None:
        """Monitor running tasks"""
        pass

    @abc.abstractmethod
    def terminate(self):
        """Handle SIGTERM"""
        pass

    @abc.abstractmethod
    def shutdown(self) -> bool:
        """Handle SIGINT"""
        pass

    @property
    @abc.abstractmethod
    def slots(self):
        """How many tasks should the manager be given to start?"""
        pass


class Worker(TaskManager):
    """A local task executor"""

    process: typing.Optional[subprocess.Popen] = None
    task_run: typing.Optional[models.TaskRun] = None

    @transaction.atomic
    def start_task(self, task_run):
        """Run only one task locally"""

        assert not self.process, "Only one task can run at a time"
        command = [
            "workflows",
            "--log-json",
            self.registry.config_module,
            "run",
            task_run.dag_run.dag,
            "-o",
            task_run.task,
        ]
        environ = {**os.environ, **task_run.dag_run.parameters}
        self.process = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            start_new_session=True,
            env=environ,
        )
        self.task_run = task_run
        self.task_run.host = socket.gethostname()
        self.task_run.mark_started(self.registry)

    def update_tasks(self, running_tasks: models.models.QuerySet):
        # TODO: check for orphaned tasks
        if not self.process:
            return
        status = self.process.poll()
        for log_msg in self.process.stdout.readlines():
            try:
                log_msg = json.loads(log_msg)["message"]
            except (json.JSONDecodeError, KeyError):
                pass
            LOGGER.info("%s logged: %s", self.task_run, log_msg)
        if status is None:
            return []
        if status == 0:
            LOGGER.info("%s finished successfully", self.task_run)
            self.task_run.mark_succeeded(self.registry)
        else:  # non-zero exit code
            LOGGER.info("%s failed with exit code %s", self.task_run, status)
            self.task_run.mark_failed(self.registry)
        self.task_run = self.process = None

    def terminate(self):
        if self.process:
            # setpgrp above sets the PGID to the subprocess' PID
            os.killpg(self.process.pid, signal.SIGKILL)
            LOGGER.warning("Terminated task run %s", self.task_run)

    def shutdown(self):
        if self.process:
            # setpgrp above sets the PGID to the subprocess' PID
            os.killpg(self.process.pid, signal.SIGINT)
            LOGGER.warning("Sent SIGINT to task run %s", self.task_run)
            return False
        else:
            return True

    @property
    def slots(self):
        # we can only run one process at a time
        return 0 if self.process else 1


class Kubernetes(TaskManager):
    """
    A task manager that runs tasks as pods on Kubernetes

    See client docs: https://github.com/kubernetes-client/python/blob/master/kubernetes/README.md
    """

    def __init__(self, **kwargs):
        from kubernetes import client, config

        super().__init__(**kwargs)
        if self.registry.worker_config.get("kube_in_cluster", True):
            config.load_incluster_config()
        else:
            config.load_kube_config()
        self.client = client.CoreV1Api()
        self.has_lock = False
        self.registry.worker_config.setdefault("kube_namespace", "default")

    @property
    def slots(self):
        if not self.has_lock:
            self.has_lock = try_advisory_lock("kubernetes")
        return 20 if self.has_lock else 0

    @staticmethod
    def _safe_label(parts: [str]) -> str:
        """Remove non-alphanumeric characters and truncate if needed"""
        assert isinstance(parts, list)
        max_length = 63
        label = "-".join(re.sub(r"[^a-z0-9]", "", part.lower()) for part in parts)
        if len(label) > max_length:
            short_hash = hashlib.md5(label.encode()).hexdigest()[:9]
            label = "-".join([label[:53], short_hash])
        return label

    def start_task(self, task_run):
        from kubernetes.client import exceptions

        pod = self.make_pod(task_run)
        task_run.host = pod.metadata.name
        try:
            resp = self.client.create_namespaced_pod(body=pod, namespace=pod.metadata.namespace)
        except exceptions.ApiException as exc:
            LOGGER.warning("Pod launch failed %s", exc)
            task_run.mark_failed(self.registry)
            return

        LOGGER.debug("Pod launched %s", resp)
        task_run.mark_started(self.registry)

    def make_pod(self, task_run: models.TaskRun):
        """
        Create the kubernetes pod object. Subclass and override this method to adjust
        the pod that is created.
        """
        from kubernetes.client import models as k8s

        name = self._safe_label(
            [
                task_run.dag_run.dag,
                task_run.task,
                task_run.dag_run.created.isoformat()[:19],
                str(task_run.attempt),
            ]
        )
        worker_config = self.get_worker_config(task_run)
        return k8s.V1Pod(
            metadata=k8s.V1ObjectMeta(
                namespace=worker_config["kube_namespace"],
                annotations={
                    "try_number": str(task_run.attempt),
                },
                name=name,
                labels={
                    "dag_name": self._safe_label([task_run.dag_run.dag]),
                    "task_name": self._safe_label([task_run.task]),
                    "execution_date": self._safe_label(
                        [task_run.dag_run.created.isoformat()[:19]]
                    ),
                },
            ),
            spec=k8s.V1PodSpec(
                containers=[
                    k8s.V1Container(
                        name="runner",
                        command=worker_config.get("kube_command"),
                        args=[
                            "workflows",
                            "--log-json",
                            self.registry.config_module,
                            "run",
                            task_run.dag_run.dag,
                            "-o",
                            task_run.task,
                        ],
                        image=worker_config["kube_image"],
                        env=[
                            {"name": name, "value": value}
                            for name, value in worker_config.get("kube_env", {}).items()
                        ],
                        resources={
                            kind: {
                                rsrc: worker_config.get(f"kube_{kind}_{rsrc}")
                                for rsrc in ["cpu", "memory"]
                                if worker_config.get(f"kube_{kind}_{rsrc}")
                            }
                            for kind in ["requests", "limits"]
                        },
                    )
                ],
                security_context=worker_config.get("kube_security_context"),
                service_account_name=worker_config.get("kube_service_account_name"),
                node_selector=worker_config.get("kube_node_selector"),
                tolerations=worker_config.get("kube_tolerations"),
                restart_policy="Never",
            ),
        )

    def update_tasks(self, running_tasks: typing.List[models.TaskRun]):
        from kubernetes.client import exceptions

        for task_run in running_tasks:
            worker_config = self.get_worker_config(task_run)
            try:
                resp = self.client.read_namespaced_pod(
                    name=task_run.host, namespace=worker_config["kube_namespace"]
                )
            except exceptions.ApiException as exc:
                LOGGER.warning("Pod status exception %s", exc)
                task_run.mark_failed(self.registry)
            else:
                if resp.status.phase == "Failed":
                    LOGGER.warning("Pod finished with a failure %s", resp.status.to_dict())
                    task_run.mark_failed(self.registry)
                elif resp.status.phase == "Succeeded":
                    LOGGER.debug("Pod finished with a success")
                    task_run.mark_succeeded(self.registry)
                else:  # pragma: no cover see https://github.com/nedbat/coveragepy/issues/198
                    continue  # still running

            # cleanup finished pods
            try:
                self.client.delete_namespaced_pod(
                    name=task_run.host, namespace=worker_config["kube_namespace"]
                )
            except exceptions.ApiException as exc:
                LOGGER.warning("Pod deletion exception %s", exc)

    def terminate(self):
        pass

    def shutdown(self) -> bool:
        # nothing is running locally so we can always shutdown quickly
        return True
